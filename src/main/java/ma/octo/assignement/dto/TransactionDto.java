package ma.octo.assignement.dto;

import lombok.Data;
import ma.octo.assignement.bo.BAccount;

import java.math.BigDecimal;
import java.util.Date;

@Data
public abstract class TransactionDto {
    private String motif;
    private BigDecimal montant;
    private Date date;
}

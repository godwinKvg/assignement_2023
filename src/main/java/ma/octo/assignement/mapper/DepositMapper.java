package ma.octo.assignement.mapper;

import lombok.Data;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.bo.Deposit;

@Data
public class DepositMapper {
    private static DepositDto depositDto;

    public static DepositDto map(Deposit deposit) {
        depositDto = new DepositDto();
        depositDto.setCompteBeneficiaire(deposit.getCompteBeneficiaire());
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifDeposit());
        depositDto.setMontant(deposit.getMontant());
        depositDto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());


        return depositDto;

    }

    public static Deposit map(DepositDto depositDto, BAccount account){
        Deposit deposit = new Deposit();
        deposit.setMotifDeposit(depositDto.getMotif());
        deposit.setMontant(depositDto.getMontant());
        deposit.setCompteBeneficiaire(account);
        deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());
        deposit.setDateExecution(depositDto.getDate());
        return deposit;
    }
}

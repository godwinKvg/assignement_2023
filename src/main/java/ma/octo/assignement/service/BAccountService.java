package ma.octo.assignement.service;

import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.bo.BAccount;

import java.util.List;

public interface BAccountService {
    List<BAccount> getAccounts();

    BAccount save(BAccount a);

    BAccount getAccount(long id);

    BAccount update(BAccount account) throws AccountNotFoundException;

}

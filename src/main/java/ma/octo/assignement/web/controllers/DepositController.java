package ma.octo.assignement.web.controllers;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.bo.Deposit;
import ma.octo.assignement.service.DepositService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deposits/")
public class DepositController {

    DepositService depositService;

    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }


    @GetMapping("")
    List<Deposit> getDeposits() {
        return depositService.findAll();
    }


    @PostMapping("")
    void createDeposit(@RequestBody DepositDto depositDto) throws AccountNotFoundException, TransactionException {
        depositService.createDeposit(depositDto);
    }
}

package ma.octo.assignement.service;
import ma.octo.assignement.bo.AppRole;
import ma.octo.assignement.bo.AppUser;

import java.util.List;



public interface AccountService {
    AppUser addNewUser(AppUser u);

    AppRole addNewRole(AppRole r);

    void addRoleToUser(String username,String rolename);

    AppUser loadUserByUsername(String username);

    List<AppUser> listUsers();

}
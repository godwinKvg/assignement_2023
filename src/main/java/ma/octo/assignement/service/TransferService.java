package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.bo.Transfer;

import java.util.List;


public interface TransferService {

    //Transfer save(Transfer t);
    List<Transfer> findAll();
    void createTransfer(TransferDto transferDto) throws InsuffisantBalanceException, AccountNotFoundException, TransactionException;
}

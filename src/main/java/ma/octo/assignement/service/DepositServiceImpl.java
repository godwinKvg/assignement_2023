package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.bo.Deposit;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.utils.TransactionValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
@Service
public class DepositServiceImpl implements DepositService{;

    AuditService auditService;
    DepositRepository depositRepository;
    AccountRepository accountRepository;

    public DepositServiceImpl(AuditService auditService, DepositRepository depositRepository, AccountRepository accountRepository) {
        this.auditService = auditService;
        this.depositRepository = depositRepository;
        this.accountRepository = accountRepository;

    }

    @Override
    public void createDeposit(DepositDto depositDto) throws AccountNotFoundException, TransactionException {
        BAccount account = accountRepository.findByRib(depositDto.getCompteBeneficiaire().getRib());

        TransactionValidator.validateDeposit(depositRepository.countAllByCompteBeneficiaireAndDateExecution(account,new Date()),account,depositDto);

        account.setSolde(account.getSolde().add(depositDto.getMontant()));

        Deposit deposit = DepositMapper.map(depositDto,account);

        depositRepository.save(deposit);

        auditService.deposit(depositDto);

    }

    @Override
    public List<Deposit> findAll() {
        return depositRepository.findAll();
        }
}

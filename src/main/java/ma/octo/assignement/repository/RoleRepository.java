package ma.octo.assignement.repository;

import ma.octo.assignement.bo.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<AppRole,Long> {
    AppRole findByRoleName(String role);
}

package ma.octo.assignement.repository;

import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.bo.Deposit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;


public interface DepositRepository extends JpaRepository<Deposit,Long> {
    int countAllByCompteBeneficiaireAndDateExecution(BAccount account, Date date);
}

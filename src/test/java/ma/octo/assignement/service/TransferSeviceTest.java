package ma.octo.assignement.service;

import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.bo.AppUser;
import ma.octo.assignement.bo.Transfer;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferSeviceTest {

    private Transfer transfer;
    private BAccount account1;
    private BAccount account2;

    private AppUser appUser1;
    private AppUser appUser2;


    @InjectMocks
    private TransferSeviceImpl transferService;

    @Mock
    TransferRepository transferRepository;
    @Mock
    AuditService auditService;
    @Mock
    DepositRepository depositRepository;
    @Mock
    AccountRepository accountRepository;


    @BeforeEach
    void initialize(){
        appUser1 = new AppUser();
        appUser1.setUsername("user1");
        appUser1.setLastname("last1");
        appUser1.setFirstname("first1");
        appUser1.setGender("Male");


        appUser2 = new AppUser();
        appUser2.setUsername("user2");
        appUser2.setLastname("last2");
        appUser2.setFirstname("first2");
        appUser2.setGender("Female");


        account1 = new BAccount();
        account1.setId(1L);
        account1.setNrCompte("010000A000001000");
        account1.setRib("RIB1");
        account1.setSolde(BigDecimal.valueOf(20000));
        account1.setUtilisateur(appUser1);


        account2 = new BAccount();
        account2.setId(2L);
        account2.setNrCompte("010000B025001000");
        account2.setRib("RIB2");
        account2.setSolde(BigDecimal.valueOf(14000));
        account2.setUtilisateur(appUser2);


        transfer = new Transfer();
        transfer.setMotifTransfer("Transfer d'argent pour une urgence !");
        transfer.setMontantTransfer(BigDecimal.valueOf(2000));
        transfer.setCompteBeneficiaire(account2);
        transfer.setCompteEmetteur(account1);
        transfer.setDateExecution(new Date());

    }

    @AfterEach
    void destroy(){
        account1=null;
        account2=null;

        appUser1=null;
        appUser2=null;
    }

    @DisplayName("It Should raise an AccountNotFoundException since the sender account is not porvided")
    @Test
    public void testTransferSenderAccountNotFoundException(){

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Compte de l'emetteur Non existant");

    }

    @DisplayName("It Should raise an AccountNotFoundException since the reciever account is null")
    @Test
    public void testTransferRecieverAccountNotFoundException(){

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(null);

        AccountNotFoundException accountNotFoundException = assertThrows(AccountNotFoundException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(accountNotFoundException.getMessage(),"Compte du receveur Non existant");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is zero or null")
    @Test
    public void testTransferZeroMoneyException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(0));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Montant vide");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is less than the minimum value")
    @Test
    public void testTransferInsuffiscientMoneyException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Montant minimal de transfer non atteint");

    }

    @DisplayName("It Should raise an TransactionException since the transaction money is greater than the maximum barrier")
    @Test
    public void testTransferMaximumMoneyReachedException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMontantTransfer(BigDecimal.valueOf(2000000));

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Montant maximal de transfer dépassé");

    }


    @DisplayName("It Should raise an InsuffisantException since the sender's money is less than the transaction money")
    @Test
    public void testTransferInsuffisantBalanceException(){
        account1.setSolde(BigDecimal.valueOf(300));

        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        InsuffisantBalanceException transactionException = assertThrows(InsuffisantBalanceException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Solde de l'emetteur insuffisant");

    }


    @DisplayName("It Should raise an TransactionException since the transaction motif is not given")
    @Test
    public void testTransferTransactionMotifException(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        transfer.setMotifTransfer(null);

        TransactionException transactionException = assertThrows(TransactionException.class,()->{
            transferService.createTransfer(TransferMapper.map(transfer));
        });

        assertEquals(transactionException.getMessage(),"Motif vide");

    }


    @DisplayName("It Should reduce the amount of money transfered from the sender and add it to the reciever")
    @Test
    public void testTransferHasBeenSuccessfull(){
        when(accountRepository.findByNrCompte("010000A000001000")).thenReturn(account1);
        when(accountRepository.findByNrCompte("010000B025001000")).thenReturn(account2);

        try {
            BigDecimal expectedAccount1Sold = account1.getSolde().subtract(transfer.getMontantTransfer());
            BigDecimal expectedAccount2Sold =account2.getSolde().add(transfer.getMontantTransfer());
            transferService.createTransfer(TransferMapper.map(transfer));
            assertEquals(expectedAccount1Sold,account1.getSolde());
            assertEquals(expectedAccount2Sold,account2.getSolde());

        } catch (TransactionException e) {
            throw new RuntimeException(e);
        } catch (InsuffisantBalanceException e) {
            throw new RuntimeException(e);
        } catch (AccountNotFoundException e) {
            throw new RuntimeException(e);
        }


    }



}
package ma.octo.assignement.service;

import ma.octo.assignement.bo.Audit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.utils.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImpl implements AuditService{

    Logger LOGGER = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Autowired
    private AuditRepository auditRepository;

    public void transfer(TransferDto transferDto) {
        Audit audit = new Audit();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
        auditRepository.save(audit);
    }


    public void deposit(DepositDto deposit) {
        Audit audit = new Audit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage("Depot sur le compte " + deposit.getCompteBeneficiaire().getNrCompte() + " par " + deposit
                .getNomPrenomEmetteur() + " d'un montant de " + deposit.getMontant()
                .toString());
        auditRepository.save(audit);
    }
}

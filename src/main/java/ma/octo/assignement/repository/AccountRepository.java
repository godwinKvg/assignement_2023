package ma.octo.assignement.repository;

import ma.octo.assignement.bo.BAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<BAccount, Long> {
  BAccount findByRib(String rib);
  BAccount findByNrCompte(String nrCompte);

}

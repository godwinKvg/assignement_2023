package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.bo.Transfer;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.utils.TransactionValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


@Transactional
@Service
public class TransferSeviceImpl implements TransferService{

    AuditService auditService;
    TransferRepository transferRepository;
    AccountRepository accountRepository;

    TransactionValidator transactionValidator;

    public TransferSeviceImpl(AuditService auditService, TransferRepository transferRepository, AccountRepository accountRepository) {
        this.auditService = auditService;
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
    }
    @Override
    public List<Transfer> findAll() {
        return transferRepository.findAll();
    }

    public void createTransfer(TransferDto transferDto)
            throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {

        BAccount senderAccount = accountRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        BAccount recieverAccount = accountRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        TransactionValidator.validateTransfert(senderAccount,recieverAccount,transferDto);

        senderAccount.setSolde(senderAccount.getSolde().subtract(transferDto.getMontant()));

        recieverAccount.setSolde(new BigDecimal(recieverAccount.getSolde().intValue() + transferDto.getMontant().intValue()));

        Transfer transfer = TransferMapper.map(transferDto,recieverAccount,senderAccount);

        transferRepository.save(transfer);

        auditService.transfer(transferDto);
    }
}

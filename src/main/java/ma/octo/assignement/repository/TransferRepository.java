package ma.octo.assignement.repository;

import ma.octo.assignement.bo.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepository extends JpaRepository<Transfer, Long> {
}

package ma.octo.assignement.service;

import ma.octo.assignement.bo.AppRole;
import ma.octo.assignement.bo.AppUser;
import ma.octo.assignement.repository.RoleRepository;
import ma.octo.assignement.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class AccountServiceImpl implements AccountService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public AccountServiceImpl(UserRepository ur, RoleRepository rr, PasswordEncoder pe) {
        userRepository = ur;
        roleRepository = rr;
        passwordEncoder = pe;
    }

    @Override
    public AppUser addNewUser(AppUser u) {
        String pw = u.getPassword();
        u.setPassword(passwordEncoder.encode(pw));
        return userRepository.save(u);
    }

    @Override
    public AppRole addNewRole(AppRole r) {
        return roleRepository.save(r);
    }

    @Override
    public void addRoleToUser(String username, String rolename) {
        AppUser u = userRepository.findByUsername(username);
        AppRole r = roleRepository.findByRoleName(rolename);
        u.getRoles().add(r);
    }

    @Override
    public AppUser loadUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<AppUser> listUsers() {
        return userRepository.findAll();
    }
}


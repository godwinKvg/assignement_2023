package ma.octo.assignement.web.controllers;
import ma.octo.assignement.bo.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/transfers/")
public
class TransferController {

    private TransferService transferService;

    public TransferController(TransferService transferService) {
        this.transferService = transferService;
    }


    @GetMapping("")
    List<Transfer> getTransfers() {
        return transferService.findAll();
    }


    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws InsuffisantBalanceException, AccountNotFoundException, TransactionException {
       transferService.createTransfer(transferDto);
    }
}

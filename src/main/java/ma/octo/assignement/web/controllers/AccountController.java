package ma.octo.assignement.web.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.dto.RoleToUserForm;
import ma.octo.assignement.bo.AppRole;
import ma.octo.assignement.bo.AppUser;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.AccountServiceImpl;
import ma.octo.assignement.utils.JWTUtils;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class AccountController {
    private AccountService accountService;

    public AccountController(AccountServiceImpl accountService) {
        this.accountService = accountService;
    }

    @GetMapping("users")
    @PostAuthorize("hasAutority('ADMIN')")
    List<AppUser> appUsers(){
        return  accountService.listUsers();
    }

    @PostMapping("users")
    @PostAuthorize("hasAutority('ADMIN')")
    AppUser saveUser(@RequestBody AppUser u){
        return accountService.addNewUser(u);
    }

    @PostMapping("roles")
    @PostAuthorize("hasAutority('ADMIN')")
    AppRole saveRole (@RequestBody AppRole r){
        return accountService.addNewRole(r);
    }

    @PostMapping("addRoleToUser")
    @PostAuthorize("hasAutority('ADMIN')")
    void addRoleToUser(@RequestBody RoleToUserForm ru){
        accountService.addRoleToUser(ru.getUsername(),ru.getRolename());
    }



    @GetMapping("/refreshToken")
    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws Exception{

        String jwtAuthorizationToken = request.getHeader(JWTUtils.AUTH_HEADER);

        if (jwtAuthorizationToken!=null && jwtAuthorizationToken.startsWith(JWTUtils.PREFIX)){
            try {// La vérification du token génère des exceptions c'est pr cela nous avons besoin du bloc try...catch

                String jwt = jwtAuthorizationToken.substring(JWTUtils.PREFIX.length());
                Algorithm algorithm = Algorithm.HMAC256(JWTUtils.SECRET);

                // Vérification et récupération des informations du token
                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(jwt);
                String username = decodedJWT.getSubject();

                AppUser appUser = accountService.loadUserByUsername(username);

                String jwtAccessToken = JWT.create()
                        .withSubject(appUser.getUsername())
                        .withExpiresAt(new Date(System.currentTimeMillis() + JWTUtils.EXPIRE_ACCESS_TOKEN))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("roles", appUser.getRoles()
                                .stream().map(ga -> ga.getRoleName())
                                .collect(Collectors.toList())).sign(algorithm);


                Map<String,String> idToken = new HashMap<>();
                idToken.put("access-token",jwtAccessToken);
                idToken.put("refresh-token",jwt);
                response.setContentType("application/json");
                new ObjectMapper().writeValue(response.getOutputStream(),idToken);

            }catch (Exception e){
                throw e;
            }

        }
        else{
            throw new RuntimeException("Refresh token required");
        }


        }


    // Comment connaître l'utilisateur authentifié
    @GetMapping("profile")
    AppUser getProfile(Principal principal){
        return accountService.loadUserByUsername(principal.getName());
    }


}

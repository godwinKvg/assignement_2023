package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.bo.Deposit;

import java.util.List;

public interface DepositService {
    void createDeposit(DepositDto depositDto) throws AccountNotFoundException, TransactionException;

    List<Deposit> findAll();
}

package ma.octo.assignement.dto;

import lombok.Data;
import ma.octo.assignement.bo.BAccount;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class DepositDto extends TransactionDto{
    private String nomPrenomEmetteur;
    private BAccount compteBeneficiaire;
}

package ma.octo.assignement.web.controllers;

import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.service.BAccountService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/accounts/")
@RestController
public class BAccountController {

    BAccountService accountService;

    public BAccountController(BAccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("all")
    List<BAccount> getAccounts() {
        return accountService.getAccounts();
    }

    @GetMapping("id")
    BAccount getUsers(@PathVariable long id) {
        return accountService.getAccount(id);
    }


    @PostMapping("create")
    BAccount create(@RequestBody BAccount account){
        return accountService.save(account);
    }


    @PostMapping("update")
    BAccount update(@RequestBody BAccount account) throws AccountNotFoundException {
        return accountService.update(account);
    }
}

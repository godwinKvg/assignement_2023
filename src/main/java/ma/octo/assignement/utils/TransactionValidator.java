package ma.octo.assignement.utils;


import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.InsuffisantBalanceException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;


public class TransactionValidator {
    static Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    private static final int MONTANT_MAXIMAL = 10000;
    private static final int MONTANT_MINIMAL = 10;

    public static void validateTransfert(BAccount senderAccount, BAccount recieverAccount, TransferDto transferDto)throws InsuffisantBalanceException, AccountNotFoundException, TransactionException{

        if (senderAccount == null) {
            LOGGER.error("Compte de l'emetteur Non existant");
            throw new AccountNotFoundException("Compte de l'emetteur Non existant");
        }

        if (recieverAccount == null) {
            LOGGER.error("Compte du receveur Non existant");
            throw new AccountNotFoundException("Compte du receveur Non existant");
        }

        if (transferDto.getMontant() == null || transferDto.getMontant().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");

        } else if (transferDto.getMontant().intValue() < MONTANT_MINIMAL) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");


        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif() == null || transferDto.getMotif().length() <1) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (senderAccount.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'emetteur");
            throw new InsuffisantBalanceException();
        }
    }

    public static void validateDeposit(int numberOfDepositOfTaday, BAccount account, DepositDto depositDto) throws TransactionException,AccountNotFoundException{

        if (numberOfDepositOfTaday>10){
            throw new TransactionException("Nombre maximal de transaction effectué");
        }

        if (account==null){
            LOGGER.warn("Compte Non existant");
            throw new AccountNotFoundException();
        }
        if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL){
            LOGGER.warn("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        // Librairies de validation de StringUtils : apache.common
        if (StringUtils.isEmpty(depositDto.getNomPrenomEmetteur())){
            LOGGER.warn("Nom du depositaire non existant");
            throw new TransactionException("Nom du depositaire non existant");
        }

        if (StringUtils.isEmpty(depositDto.getMotif())) {
            LOGGER.warn("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }
}

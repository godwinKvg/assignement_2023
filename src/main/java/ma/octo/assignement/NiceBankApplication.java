package ma.octo.assignement;

import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.bo.*;
import ma.octo.assignement.service.AccountService;
import ma.octo.assignement.service.BAccountService;
import ma.octo.assignement.service.DepositService;
import ma.octo.assignement.service.TransferService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication {

    public static void main(String[] args) {
        SpringApplication.run(NiceBankApplication.class, args);
    }

    @Bean
    CommandLineRunner start(AccountService accountService, BAccountService bAccountService, TransferService transferService, DepositService depositService) {

        return args -> {

            accountService.addNewRole(new AppRole(null,"ADMIN"));
            accountService.addNewRole(new AppRole(null,"USER"));

            AppUser appUser1 = accountService.addNewUser(new AppUser(null,"user1","1234","Male","last1","first1",new Date(),new ArrayList<>()));
            AppUser appUser2 = accountService.addNewUser(new AppUser(null,"user2","1234","Female","last2","first1",new Date(),new ArrayList<>()));
            AppUser admin = accountService.addNewUser(new AppUser(null,"admin","1234","Female","last2","first1",new Date(),new ArrayList<>()));

            accountService.addRoleToUser("user1","USER");
            accountService.addRoleToUser("user2","USER");
            accountService.addRoleToUser("admin","ADMIN");
            accountService.addRoleToUser("admin","USER");

            BAccount account1 = new BAccount();
            account1.setNrCompte("010000A000001000");
            account1.setRib("RIB1");
            account1.setSolde(BigDecimal.valueOf(200000L));
            account1.setUtilisateur(appUser1);

            account1 = bAccountService.save(account1);

            BAccount account2 = new BAccount();
            account2.setNrCompte("010000B025001000");
            account2.setRib("RIB2");
            account2.setSolde(BigDecimal.valueOf(140000L));
            account2.setUtilisateur(appUser2);

            account2 = bAccountService.save(account2);

            BAccount account3 = new BAccount();
            account3.setNrCompte("0100A00025001000");
            account3.setRib("RIB2");
            account3.setSolde(BigDecimal.valueOf(140000L));
            account3.setUtilisateur(appUser2);

            bAccountService.save(account3);


            Transfer transfer = new Transfer();
            transfer.setMotifTransfer("Transfer d'argent pour une urgence !");
            transfer.setMontantTransfer(BigDecimal.valueOf(2000));
            transfer.setCompteBeneficiaire(account2);
            transfer.setCompteEmetteur(account1);
            transfer.setDateExecution(new Date());

            transferService.createTransfer(TransferMapper.map(transfer));

            Deposit deposit = new Deposit();
            deposit.setMotifDeposit("Depot d'argent pour urgence !");
            deposit.setMontant(BigDecimal.valueOf(2000L));
            deposit.setCompteBeneficiaire(account1);
            deposit.setNomPrenomEmetteur("Aboudou");
            deposit.setDateExecution(new Date());

            depositService.createDeposit(DepositMapper.map(deposit));

        };
    }
}

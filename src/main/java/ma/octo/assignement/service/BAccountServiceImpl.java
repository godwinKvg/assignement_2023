package ma.octo.assignement.service;

import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BAccountServiceImpl implements BAccountService {

    AccountRepository accountRepository;

    public BAccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public BAccount save(BAccount a) {
        return accountRepository.save(a);
    }

    @Override
    public BAccount getAccount(long id) {
        return accountRepository.getById(id);
    }

    @Override
    public BAccount update(BAccount account) throws AccountNotFoundException {
        BAccount account1 = null;

        if (account!=null && account.getId()!=0){
            account1 = accountRepository.getById(account.getId());

            if (account1 != null){
                account1.setSolde(account.getSolde()!=null?account.getSolde():account1.getSolde());
            }else {
                throw new AccountNotFoundException();
            }
        }else {
            throw new AccountNotFoundException();
        }

        return account1;

    }


    @Override
    public List<BAccount> getAccounts() {
        return accountRepository.findAll();
    }
}

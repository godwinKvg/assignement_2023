package ma.octo.assignement.exceptions;

public class InsuffisantBalanceException extends Exception {

  private static final long serialVersionUID = 1L;

  public InsuffisantBalanceException() {
    super("Solde de l'emetteur insuffisant");
  }
}

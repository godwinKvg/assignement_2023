package ma.octo.assignement.mapper;

import lombok.Data;
import ma.octo.assignement.bo.BAccount;
import ma.octo.assignement.bo.Transfer;
import ma.octo.assignement.dto.TransferDto;

@Data
public class TransferMapper {

    private static TransferDto transferDto;

    public static TransferDto map(Transfer transfer) {
        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());
        transferDto.setMontant(transfer.getMontantTransfer());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        return transferDto;

    }

    public static Transfer map(TransferDto transferDto, BAccount recieverAccount,BAccount senderAccount){
        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(recieverAccount);
        transfer.setCompteEmetteur(senderAccount);
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setMotifTransfer(transferDto.getMotif());

        return  transfer;
    }
}

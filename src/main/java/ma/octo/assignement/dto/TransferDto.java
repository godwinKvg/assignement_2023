package ma.octo.assignement.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class TransferDto extends TransactionDto {
  private String nrCompteEmetteur;
  private String nrCompteBeneficiaire;
}
